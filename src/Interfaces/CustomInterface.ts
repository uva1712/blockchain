
export interface ICustomResponse{
    code : number;
    data : any;
    error : string | null;
}


import { Block } from "./Block";
import { Transaction } from "./Transaction";

export class Blockchain {
    public chain: Block[];
  
    constructor() {
      this.chain = [this.createGenesisBlock()];
    }
  
    /**
     * Create the initial block for the blockchain.
     */
    private createGenesisBlock() {
      return new Block("", new Transaction(null, "", 0));
    }
  
    /**
     * Get the latest block in the chain.
     */
    private get latestBlock() {
      return this.chain[this.chain.length - 1];
    }
  
    /**
     * Add a new transaction to the blockchain. The transaction is placed in a new block.
     */
    public addTransaction(transaction: Transaction) : Block {
      const newBlock = new Block(
        this.latestBlock.hash,
        transaction,
        Date.now()
      );
      this.chain.push(newBlock);
      return newBlock;
    }
  
    /**
     * Get the balance of an address by aggregating the amounts in the related transactions.
     */
    public getBalance(address: string) {
      let balance = 0;
      for (const block of this.chain) {
        const { fromAddress, toAddress, amount } = block.transaction;
        if (fromAddress === address) {
          balance -= amount;
        }
        if (toAddress === address) {
          balance += amount;
        }
      }
      return balance;
    }
  }
export interface TransactionLog{
    timpestamp : Date;
    fromAddress : string | null;
    toAddress : string;
    amount : number;
    event : LogEvent;
    description ?: string
}



export enum LogEvent {
        SUCCESS = 'success',
        FAILED = 'failed',
        STARTED = 'started',
        ENDED = 'ended',
        BLOCKCREATED = 'block_created',
        TRANSACTIONCREATED = 'transaction_created'
}
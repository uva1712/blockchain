import {Pool} from 'pg';
import { MongoClient } from 'mongodb';

/** Database pool configuration */
export const pool : Pool = new Pool({connectionString:process.env.POSTGRES_URL})

const url = process.env.MONGODB_URL ?? 'mongodb://localhost:27017'
export const client : MongoClient = new MongoClient(url);

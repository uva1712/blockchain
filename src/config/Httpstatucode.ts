export enum HttpStatusCode{
    OK = 200, // Ok response
    CREATED = 201, // data created
    BAD_REQUEST = 400, // invalid req body properties
    NOT_FOUND = 404, // invalid endpoints
    INTERNAL_SERVER_ERROR = 500 // internal server error if any
}
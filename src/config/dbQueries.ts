

/** query to the balance for the given addrress */
const getBalanceQuery = 'select sum(amount) - coalesce((select sum(amount) from transactions where from_address = $1),0) as balance from transactions where to_address=$1';

/** query to get all the transactions for the given address */
const getTransactionsQuery = 'select * from transactions where from_address=$1 or to_address=$1'

/** query to insert the transaction refering the block for which the transaction is created */
const addTransaction = 'insert into transactions (block_id,to_address,from_address,amount,created_at,updated_at) values ($1,$2,$3,$4,$5,$6)'

/** query to create block to store the transaction and on successfull creation of the block it will return the id (Primary key) */
const addBlock = 'insert into block(prevhash,timestamp,nonce,created_at,updated_at) values($1,$2,$3,$4,$5) returning id'

/** query to get all the blocks and their respective transactions */
const getBlocks = `select b.prevhash,b.nonce,b.timestamp, (select json_build_object('fromAddress',t.from_address,'toAddress',t.to_address,'amount',t.amount) as transaction from transactions t where t.block_id=b.id) from block b`;

/** query to insert an user */
const insertUser = `insert into users(address,email,name,created_at,updated_at) values ($1,$2,$3,$4,$5)`;


export default {
    getBalanceQuery,
    getTransactionsQuery,
    addTransaction,
    addBlock,
    getBlocks,
    insertUser
} as const
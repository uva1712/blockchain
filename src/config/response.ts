import { ICustomResponse } from "../Interfaces/CustomInterface"


/**Generates Custom response body  based on the value passed which is of type ICustomResponse
 * @param customResponse 
*/
export function CustomResponse(customResponse : ICustomResponse){
  return {
    header : {
      code : customResponse.code 
    },
    body : {
        value : customResponse.data,
        error : customResponse.error
    }
  }
}
export enum CustomStatusCode{
    SUCCESS = 600, // Data exists in database for the request
    NO_DATA_FOUND = 602,  // No data exists for the given request
    FIELD_ERROR = 604, // request body field validation error
    SERVER_ERROR = 700, // server error e.g. Database errors, business logic etc.
    DUPLICATION = 615, // error code for duplicate values e.g. unique_key_violates etc,.
    INVALID_ADDRESS = 715 // error code for in valid address of the user which is not exists
}
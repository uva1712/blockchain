import express from 'express';
import * as TransactionService from "../services/TransactionService"
import { addressvalidatorMiddlerware } from '../middlewares/addressvalidator';
import { transactionRequestMiddleware } from '../middlewares/transactionvalidator';


let transactionRouter = express.Router();

/** Route to get the balance of the address */
transactionRouter.get('/balance/:address',addressvalidatorMiddlerware,TransactionService.getBalance)

/** Route to get the transactions for the address */
transactionRouter.get('/:address',addressvalidatorMiddlerware,TransactionService.getTransactions)

/** Route to create the block and its associated transaction*/
transactionRouter.post('/',transactionRequestMiddleware,TransactionService.createTransaction);

export default transactionRouter
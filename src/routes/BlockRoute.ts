import express from 'express';
import * as BlockService from "../services/BlockService"


let blockRouter = express.Router();

/** Routes to get all the blocks */
blockRouter.get('/',BlockService.getBlocks) 



export default blockRouter
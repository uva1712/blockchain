import { Request, Response } from "express";
import { HttpStatusCode } from "../config/Httpstatucode";
import { CustomStatusCode } from "../config/CustomStatusCode";
import { CustomResponse } from "../config/response";
import * as BlockRepo from '../repo/BlockRepo';


/**service to get all the blocks */
export async function getBlocks(_:Request,res:Response){
    try {
         let blocks = await BlockRepo.getBlocks()
         if(!blocks?.length){
            return res.status(HttpStatusCode.OK).json(CustomResponse({code:CustomStatusCode.NO_DATA_FOUND,data:blocks,error:null}))
         }
        return res.status(HttpStatusCode.OK).json(CustomResponse({code:CustomStatusCode.SUCCESS,data:blocks,error:null}))
    } catch (error : any) {
        return res.status(HttpStatusCode.INTERNAL_SERVER_ERROR).json(CustomResponse({code:CustomStatusCode.SERVER_ERROR,data:null,error:error.message}))
    }
}
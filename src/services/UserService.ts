import { Request, Response } from "express";
import { HttpStatusCode } from "../config/Httpstatucode";
import { CustomStatusCode } from "../config/CustomStatusCode";
import { CustomResponse } from "../config/response";
import * as userRepo from '../repo/UserRepo';
import { User } from "../model/User";
import { DatabaseErrorCode } from "../config/DatabaseErrorCode";


/**service to create a user*/
export async function createUser(req:Request,res:Response){
    try {
        let user : User = {address:req.body.address,email:req.body.email,name:req.body.name};
         let blocks = await userRepo.createUser(user);  
         if(blocks){
            return res.status(HttpStatusCode.CREATED).json(CustomResponse({code:CustomStatusCode.SUCCESS,data:null,error:null}))
         }
        return res.status(HttpStatusCode.OK).json(CustomResponse({code:CustomStatusCode.SUCCESS,data:blocks,error:null}))
    } catch (error : any) {
        if(error.code===DatabaseErrorCode.DUPLICATE){
            return res.status(HttpStatusCode.BAD_REQUEST).json(CustomResponse({code:CustomStatusCode.DUPLICATION,data:null,error:"This is already associated with the existing user"}))
        }
        return res.status(HttpStatusCode.INTERNAL_SERVER_ERROR).json(CustomResponse({code:CustomStatusCode.SERVER_ERROR,data:null,error:error.message}))
    }
}
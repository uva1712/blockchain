import { Request,Response } from 'express';
import * as TransactionRepo from '../repo/TransactionRepo';
import { HttpStatusCode } from '../config/Httpstatucode';
import { CustomStatusCode } from '../config/CustomStatusCode';
import { CustomResponse } from '../config/response';
import { Blockchain } from '../model/BlockChain';
import { Block } from '../model/Block';
import { Transaction } from '../model/Transaction';
import { DatabaseErrorCode } from '../config/DatabaseErrorCode';
import { storeLogs } from '../repo/LogsRepo';
import { LogEvent } from '../model/TransactionLog';


let blockchain : Blockchain = new Blockchain();


/** To get the balance of the given address
 * @param req 
 */
export async function getBalance(req:Request,res:Response) : Promise<Response>{
    try {
        let address = req.params.address;
        let result = await TransactionRepo.getBalance(address);
        console.log("the balance " ,result)
        if(!result.balance){
            return res.status(HttpStatusCode.OK).json(CustomResponse({code:CustomStatusCode.NO_DATA_FOUND,data:null,error:null}))
        }
        return res.status(HttpStatusCode.OK).json(CustomResponse({code:CustomStatusCode.SUCCESS,data:{balance:parseInt(result.balance)},error:null}))
    } catch (error:any) {
        return res.status(HttpStatusCode.INTERNAL_SERVER_ERROR).json(CustomResponse({code:CustomStatusCode.SERVER_ERROR,data:null,error:error.message}))
    }
}



/** To get the transactions of the given address
 * @param req 
 */
export async function getTransactions(req:Request,res:Response) : Promise<Response>{
    try {
        let address = req.params.address;
        let result = await TransactionRepo.getTransactions(address);
        if(result.length===0){
            return res.status(HttpStatusCode.OK).json(CustomResponse({code:CustomStatusCode.NO_DATA_FOUND,data:null,error:null}))
        }
        return res.status(HttpStatusCode.OK).json(CustomResponse({code:CustomStatusCode.SUCCESS,data:result,error:null}))
    } catch (error:any) {
        return res.status(HttpStatusCode.INTERNAL_SERVER_ERROR).json(CustomResponse({code:CustomStatusCode.SERVER_ERROR,data:null,error:error.message}))
    }
}


/** To create an transaction and block 
 * @param req 
 */
export async function createTransaction(req:Request,res:Response){
    const {fromAddress,toAddress,amount} = req.body;
    try {

        // to log the begining of the transaction
        storeLogs({amount,fromAddress,toAddress,timpestamp:new Date(),description:"The transaction started . . .",event:LogEvent.STARTED});
        let block : Block = blockchain.addTransaction(new Transaction(fromAddress,toAddress,amount));
        let result = await TransactionRepo.createTransaction(block);
        if(!result){

            // to log the failed transaction
            storeLogs({amount,fromAddress,toAddress,timpestamp:new Date(),event:LogEvent.FAILED});
            return res.status(HttpStatusCode.INTERNAL_SERVER_ERROR).json(CustomResponse({code:CustomStatusCode.SERVER_ERROR,data:null,error:null}))
        }

        // to log the creation of the new transaction
        storeLogs({amount,fromAddress,toAddress,timpestamp:new Date(),description:"Transaction created successfully . . .",event:LogEvent.SUCCESS});
        return res.status(HttpStatusCode.CREATED).json(CustomResponse({code:CustomStatusCode.SUCCESS,data:null,error:null}))
    } catch (error : any) {
         if(error.code===DatabaseErrorCode.INVALIDFOREIGN){

            // to log for non existing user 
            storeLogs({amount,fromAddress,toAddress,timpestamp:new Date(),description:"either from address or the to address not exists . . .",event:LogEvent.FAILED});
            return res.status(HttpStatusCode.BAD_REQUEST).json(CustomResponse({code:CustomStatusCode.INVALID_ADDRESS,data:null,error:"either from address or the to address not exists"}))
         }

         // to log any common error with the message of the error
         storeLogs({amount,fromAddress,toAddress,timpestamp:new Date(),description:error.message,event:LogEvent.FAILED});
        return res.status(HttpStatusCode.INTERNAL_SERVER_ERROR).json(CustomResponse({code:CustomStatusCode.SERVER_ERROR,data:null,error:error.message}))
    }finally{

        // to log the transaction ending
        storeLogs({amount,fromAddress,toAddress,timpestamp:new Date(),description:"The transaction cycle ended . . .",event:LogEvent.ENDED});
    }
}



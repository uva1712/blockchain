import { NextFunction, Request, Response } from "express";
import { HttpStatusCode } from "../config/Httpstatucode";
import { CustomResponse } from "../config/response";
import { CustomStatusCode } from "../config/CustomStatusCode";


/** Middleware to validate the request body of the transaction  */
export function transactionRequestMiddleware(req:Request,res:Response,next:NextFunction){
    if(!req.body.fromAddress || !req.body.toAddress || !req.body.amount){
        return res.status(HttpStatusCode.BAD_REQUEST).json(CustomResponse({code:CustomStatusCode.FIELD_ERROR,data:"",error:"Invalid Request Body"}))
    }

    if(typeof req.body.amount !== 'number' || req.body.amount<=0){
        return res.status(HttpStatusCode.BAD_REQUEST).json(CustomResponse({code:CustomStatusCode.FIELD_ERROR,data:"",error:"Invalid Amount"}))
    }
    next();
}
import { Request,Response,NextFunction } from "express";
import { HttpStatusCode } from "../config/Httpstatucode";
import { CustomResponse } from "../config/response";
import { CustomStatusCode } from "../config/CustomStatusCode";


/** Middleware to validate the address  */
export function addressvalidatorMiddlerware(req:Request,res:Response,next:NextFunction){
   let address = req.params.address;
   
   /** invalid address responds with Bad request */
   if( !address || !address.length || typeof address !== 'string'){
    return res.status(HttpStatusCode.BAD_REQUEST).json(CustomResponse({code : CustomStatusCode.FIELD_ERROR,data:null,error:"Invalid address"}))
   }

   next(); 
}
import express, {Request,Response,Application} from 'express';
import helmet from 'helmet';
import morgan from 'morgan';
import cors from 'cors';
import bodyParser from "body-parser";
import * as dotenv from 'dotenv';
import transactionRouter from './routes/TransactionRoute';
import { HttpStatusCode } from './config/Httpstatucode';
import blockRouter from './routes/BlockRoute';
import userRouter from './routes/UserRoute';

/** configuration of express */
dotenv.config();
const app:Application = express();
app.use(express.json())
app.use(helmet());
app.use(helmet.crossOriginResourcePolicy({policy:"cross-origin"}));
app.use(morgan("common"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));
app.use(cors());

const PORT = 8000; // port on which the node server listens for request

app.use('/transaction',transactionRouter) // base route for transaction
app.use('/blocks',blockRouter) // base route for block
app.use('/register',userRouter)


app.use("*",(_:Request,res:Response)=>{
    res.status(HttpStatusCode.NOT_FOUND).end()  // to handle the non existing endpoints
  })

/** initiating server */
app.listen(PORT, ():void => {
    console.log(`Server Running here 👉 https://localhost:${PORT}`);
});
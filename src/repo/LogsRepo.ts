import { MongoClient } from "mongodb";
import { client } from "../config/dbconfig";
import { TransactionLog } from "../model/TransactionLog";



export async function storeLogs(log:TransactionLog){
    let mongoClient : MongoClient ;
    try {
        mongoClient  = await client.connect(); 
        mongoClient.db('logs').collection('transaction_logs').insertOne(log);
    } catch (error) {
         console.log(error)
    }
}
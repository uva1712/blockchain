import { PoolClient } from "pg";

import { pool } from "../config/dbconfig";
import queries from '../config/dbQueries';
import { User } from "../model/User";



/** to create a user and returns the id of the currently created user 
 * @param user accepts User type
 */
export async function createUser(user:User){
    let dbConnection!: PoolClient;
    try {
        dbConnection = await pool.connect();
       let userRecord = await dbConnection.query(queries.insertUser,[user.address,user.email,user.name,new Date().toISOString(),new Date().toISOString()]);
       return userRecord.rowCount;
    } catch (error:any) {
        throw error
    }finally{
        dbConnection?.release();
    }
 }



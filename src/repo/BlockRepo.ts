import { PoolClient } from "pg";
import { Block } from "../model/Block";
import { pool } from "../config/dbconfig";
import queries from '../config/dbQueries';



/** to create a block and returns the id of the currently created record 
 * @param block  Block created using the block class
 * @param poolClient refernce of the currently created client to keep this execution inside the transaction(sql)
 * and to commit if the downstream queries are executed without any errors 
 */
export async function createBlock(block:Block,poolClient : PoolClient){
    try {
       let blockId = await poolClient.query(queries.addBlock,[block.prevHash,block.timestamp,block.nonce,new Date().toISOString(),new Date().toISOString()]);
       return blockId.rows[0].id
    } catch (error:any) {
        throw Error(error.message)
    }
 }


 /** to get all the blocks of the Block chain*/
 export async function getBlocks(){
    let dbConnection!:PoolClient;
    try {
        dbConnection = await pool.connect();
        let blocks = await dbConnection.query(queries.getBlocks);
        return blocks.rows;
    } catch (error:any) {
        throw Error(error.message)
    }
 }
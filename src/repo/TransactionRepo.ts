import { Request , Response } from "express";
import { pool } from "../config/dbconfig";
import queries from '../config/dbQueries';
import { Block } from "../model/Block";
import { Transaction } from "../model/Transaction";
import { Pool, PoolClient } from "pg";
import { createBlock } from "./BlockRepo";
import { storeLogs } from "./LogsRepo";
import { LogEvent } from "../model/TransactionLog";

/** to get the balance for the given address */
export async function getBalance(address: string) : Promise<any>{
    let dbConnection!: PoolClient;
    try {
        dbConnection = await pool.connect();
        let balance = await dbConnection.query(queries.getBalanceQuery,[address]);
        return balance.rows[0];
    } catch (error:any) {
        throw Error(error.message);   
    }finally{
        dbConnection?.release();
    }
}

/** to get the transactions for the given address */
export async function getTransactions(address: string) : Promise<any>{
    let dbConnection!: PoolClient;
    try {
        dbConnection = await pool.connect();
        let transactions = await dbConnection.query(queries.getTransactionsQuery,[address]);
        return transactions.rows;
    } catch (error:any) {
        throw Error(error.message);   
    }finally{
        dbConnection?.release();
    }
}


/** This accepts the block
 * @param block 
 * this repo is responsible for both successfull creation of the block and its coressponding transaction
 */
export async function createTransaction(block:Block) : Promise<number>{
    let dbConnection! : PoolClient; 
   try {
    dbConnection = await pool.connect();
    /** Hera begining the transaction */
    dbConnection.query('BEGIN')

    /** trying to create the block and to get the id of the record */
    let blockId = await createBlock(block,dbConnection);

    /** trying to create the transaction for the created block refering the block id */
    let transaction = block.transaction;
    let result  = await dbConnection.query(queries.addTransaction,[blockId,transaction.toAddress,transaction.fromAddress,transaction.amount,new Date().toISOString(),new Date().toISOString()]);
    /** Commiting the database executions */
    dbConnection.query('COMMIT')
    storeLogs({...transaction,timpestamp:new Date(),description:"The block for this transaction is created . . .",event:LogEvent.BLOCKCREATED});
    storeLogs({...transaction,timpestamp:new Date(),description:"The transaction is attached to the block . . .",event:LogEvent.TRANSACTIONCREATED});
    /** returning the rowCount to mention the  */
    return result.rowCount;
   } catch (error:any) {
    /** Rollbacking the changes if any error occurs in the database */
    dbConnection.query('ROLLBACK')
      throw error
   }finally{

    /** relasing the currently created connection */
      dbConnection?.release();
   }
}
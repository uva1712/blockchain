FROM node:14 as base

WORKDIR /app

COPY package.json ./

COPY . .

EXPOSE 8000

RUN npm run build
